//
//  Headline.swift
//  HelloSingleView
//
//  Created by David Bielenberg on 06.11.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import UIKit

class ExperienceElement {
    
    let imageName: String
    let title: String
    var year: String
    var description: String
    
    // By setting default values here we don't have to set these parameters when creating the object
    init(title: String, year: String, description: String, imageName: String) {
        self.title = title
        self.year = year
        self.description = description
        self.imageName = imageName

    }
    
    init() {
        title = "lorem"
        year = "1996"
        description = "lorem"
        imageName = "vv"
    }
    
    func getImage() -> UIImage? {
        return UIImage(named: self.imageName)
    }
    
    
}
