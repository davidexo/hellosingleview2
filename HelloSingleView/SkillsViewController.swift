import UIKit

class SkillsViewController: UIViewController {
    
    
    @IBOutlet weak var box2: UIView!
    @IBOutlet weak var box1: UIView!
    @IBOutlet weak var box3: UIView!
    @IBOutlet weak var dismissButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        pushAnimate(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeRound(view: box1)
        makeRound(view: box2)
        makeRound(view: box3)
        
       
        dismissButton.setImage(UIImage(systemName: "sun.max.fill"), for: .normal)
        dismissButton.imageView?.contentMode = .scaleAspectFit
        dismissButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        //button.setImage(icon, for: .normal)
        
    }
    
    @IBAction func pushDismiss(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var pushDismissIcon: UIImageView!
    
    @IBAction func pushAnimate(_ sender: Any) {
        print("Animate!")
        let duration = 1.0
        let delay = 0.3
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 2, options: [.curveLinear, .repeat, .autoreverse], animations: {
            self.moveTop(view: self.box1)
            self.moveTop(view: self.box2)
            self.moveTop(view: self.box3)
            
            self.box1.backgroundColor = UIColor.black
            self.box2.backgroundColor = UIColor.red
            self.box3.backgroundColor = UIColor.yellow
            
            self.box1.transform = CGAffineTransform(rotationAngle: 180)
            self.box2.transform = CGAffineTransform(rotationAngle: 180)
            self.box3.transform = CGAffineTransform(rotationAngle: 180)
            
            self.box1.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.box2.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.box3.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            
            self.makeSquare(view: self.box1)
            self.makeSquare(view: self.box2)
            self.makeSquare(view: self.box3)
            
        }) { (_) in
            // Call when animation is done
        }
    }
    
    func moveRight(view: UIView) {
        view.center.x += 100
    }
    
    func moveLeft(view: UIView) {
        view.center.x -= 100
    }
    
    func moveTop(view: UIView) {
        view.center.y -= 100
    }
    
    func moveBottom(view: UIView) {
        view.center.y += 100
    }
    
    func makeRound(view: UIView) {
        view.layer.cornerRadius = view.frame.height / 2
    }
    
    func makeSquare(view: UIView) {
        view.layer.cornerRadius = view.frame.height / 6
    }
    
    
    
}



