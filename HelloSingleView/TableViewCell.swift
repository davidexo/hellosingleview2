//
//  TableViewCell.swift
//  HelloSingleView
//
//  Created by David Bielenberg on 30.10.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var experienceTitle: UILabel!
    
    
    
}
