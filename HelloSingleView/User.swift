//
//  User.swift
//  HelloSingleView
//
//  Created by David Bielenberg on 06.11.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import UIKit

class User {
    
    var name: String
    var adress1: String
    var adress2: String
    var description: String
    var imageName: String
    
    init(name: String, adress1: String, adress2: String, description: String, imageName: String) {
        self.name = name
        self.adress1 = adress1
        self.adress2 = adress2
        self.description = description
        self.imageName = imageName
    }
    
    func getImage() -> UIImage? {
        return UIImage(named: self.imageName)
    }
    
}
