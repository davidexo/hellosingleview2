//
//  ViewController.swift
//  HelloSingleView
//
//  Created by David Bielenberg on 23.10.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var avatarContainer: UIImageView!
    
    // Buttons
    @IBOutlet weak var mainBtn: UIButton!
    @IBOutlet weak var secondaryBtn: UIButton!
    
    @IBAction func clickDismiss(_ sender: Any) {
    }
    // CV Data
    @IBOutlet weak var paragraph: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var adress: UILabel!
    
    @IBOutlet weak var bottomContainer: UIView!
    
    @IBOutlet weak var avatarImageContainer: UIView!
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // BUTTONS
        mainBtn.layer.cornerRadius = mainBtn.frame.height / 2
        secondaryBtn.layer.cornerRadius = secondaryBtn.frame.height / 2
        
        // AVATAR
        // Round the avatar image itself
        avatarContainer.layer.cornerRadius = avatarContainer.frame.height / 2
        avatarContainer.clipsToBounds = true
        
        let myCV: CV = CV(name: "David",
                          mobile: "+49 123 456 78",
                          adress1: "Toosbüystraße 7",
                          adress2: "45125 Flensburg",
                          description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.",
                          imageName: "avatar")
        
        navigationBar.title = myCV.name
        avatarImageView.image = myCV.getImage()
        paragraph.text = myCV.description
        adress.text = "\(myCV.mobile) \n \(myCV.adress1) \n \(myCV.adress2)"
        
        
    }
    
    @IBAction func pressMainButton(_ sender: Any) {
        performSegue(withIdentifier: "ExperienceSegue", sender: self)
    }
    
    @IBAction func PressSecondaryButton(_ sender: Any) {
        performSegue(withIdentifier: "SkillsSegue", sender: self)
    }
    
}

