import UIKit


class RoundedShadowView: UIView {
    // The view's height is known in this function which is why we change the cornerRadius here.
    override func layoutSubviews() {
        super.layoutSubviews()
        // Add a shadow to the view
        self.clipsToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 20)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 20
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 60).cgPath
    }
}



