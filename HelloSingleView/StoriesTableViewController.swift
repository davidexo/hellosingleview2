//
//  StoriesTableViewController.swift
//  HelloSingleView
//
//  Created by David Bielenberg on 01.11.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import UIKit

var pressedIndex = 0
var pressedSection = 0

class HeadlineTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headlineTitleLabel: UILabel!
    @IBOutlet weak var headlineTextLabel: UILabel!
    @IBOutlet weak var headlineImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


class StoriesTableViewController: UITableViewController {
    
    
    
    @IBOutlet var myTableView: UITableView!
    
    var mySections = [
        "Working Experience",
        "Acadamic Exprerience"
    ]
    
    var arr: [[ExperienceElement]] = [
        [
            ExperienceElement(title: "upljft", year: "2013 - 2014", description: "upljft – das Joint Venture von thjnk und Facelift. Eine der führenden Kreativagenturen Deutschlands und einer der erfolgreichsten Anbieter für Social Media Marketing Technologien in Europa. Das Beste aus zwei Branchen in einer Agentur zusammengebracht.", imageName: "upljft"),
            ExperienceElement(title: "deepblue Networks", year: "2013 - 2014", description: "Gegründet im Jahr 2001, beschäftigen wir heute über 150 Mitarbeiter, die ganzheitlich an digitalen Produkten, Marken und Kampagnen arbeiten. Zu unseren Kunden zählen u.a. die Unternehmen Bitburger, Bosch, Osram, Gerolsteiner, Migros, otelo, Swatch und Unilever.", imageName: "db-n"),
            ExperienceElement(title: "visuellverstehen", year: "2013 - 2014", description: "Wir designen, entwickeln, kreieren, formulieren und beraten. Vom Logo bis zum Markenauftritt. Von der Pressemitteilung bis zur Content-Strategie. Vom Onepager bis zur Webanwendung. Vom Fach und mit Fokus auf Qualität. Beispiele gefällig? Hier sind unsere Referenzen.", imageName: "vv")
        ],
        
        [
            ExperienceElement(title: "HS Flensburg", year: "2013 - 2014", description: "Medien zeigen Informationen in Form von Text, Bild, Sprache, Musik und Film. Alle diese Informationen werden heute digital dargestellt, gespeichert und verbreitet. Der Bachelor-Studiengang Medieninformatik bietet ein interdisziplinäres Studium der Informatik und der Gestaltung. Zentrale Themen sind die Software-Entwicklung von plattformübergreifenden interaktiven Anwendungen, die Erstellung von 2D- und 3D-Animationsfilmen und Game-Design. Der Studiengang legt großen Wert auf die Zusammenarbeit mit der Film- und der Spiele-Industrie. Die Medieninformatik ist einer der Initiatoren und Ausrichter der Flensburger Kurzfilmtage – einem Filmfestival und Event mit überregionaler Ausstrahlung – und organisiert Ausstellungen, auf denen studentische Projekte vorgestellt und prämiert werden. Von diesen Kooperationen profitieren beide Seiten – Studierende und Industriepartner. Dazu trägt die hochwertige Ausstattung des Studiengangs bei. So stehen viele spezielle Studios für Lehrveranstaltungen und Projektarbeiten zur Verfügung:", imageName: "hs")
        ]
        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.delegate = self
        
        print("array size: \(arr.count)")
        
        // Set height for section headers
        self.tableView.sectionHeaderHeight = 50
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return arr.count
    }
    
    // Set up a label for the Headlines of each section
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        
        
        label.frame = CGRect.init(x: 8, y: 8, width: headerView.frame.width-8, height: headerView.frame.height-8)
        label.text = mySections[section]
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor(named: "dark-text")
        
        headerView.addSubview(label)
        headerView.backgroundColor = UIColor(named: "accent")
        
        
        
        return headerView
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arr[section].count

        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
            as! HeadlineTableViewCell
        
        
        let experience: ExperienceElement = arr[indexPath.section][indexPath.row]
        
        cell.headlineTitleLabel?.text = experience.title
        cell.headlineTextLabel?.text = experience.year
        
        
        cell.headlineImageView?.image = UIImage(named: experience.imageName)
        cell.headlineImageView?.layer.cornerRadius = 16
        cell.headlineImageView?.layer.masksToBounds = true
        
        
        
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Save information on which cell was pressed
        pressedIndex = indexPath.row
        pressedSection = indexPath.section
        
        self.performSegue(withIdentifier: "ExperienceDetail1", sender: self)
        print("pressedIndex when selecting row: \(pressedIndex)")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Get variable for destination view
        let vc = segue.destination as! ExperienceViewDetailController
        // data we want to pass to the detail view
        vc.data = self.arr[pressedSection][pressedIndex]
    } 
}















