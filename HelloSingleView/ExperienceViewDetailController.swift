//
//  ExperienceViewDetailController.swift
//  HelloSingleView
//
//  Created by David Bielenberg on 30.10.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import UIKit

class ExperienceViewDetailController: UIViewController {
        
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    @IBOutlet weak var experienceImageView: UIImageView!
    @IBOutlet weak var experienceTitle: UILabel!
    @IBOutlet weak var experienceYear: UILabel!
    @IBOutlet weak var experienceDescription: UILabel!
    
    // Here we store the data that is being passed from the TableViewController
    var data: ExperienceElement = ExperienceElement()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        experienceImageView.image = data.getImage()
        experienceTitle.text = data.title
        experienceYear.text = data.year
        experienceDescription.text = data.description
        navigationBar.title = data.title
        
    }
}
