



import UIKit

class CV {
    
    var name: String
    var mobile: String
    var adress1: String
    var adress2: String
    var description: String
    var imageName: String
    
    init(name: String, mobile: String, adress1: String, adress2: String, description: String, imageName: String) {
        self.name = name
        self.adress1 = adress1
        self.adress2 = adress2
        self.mobile = mobile
        self.description = description
        self.imageName = imageName
    }
    
    func getImage() -> UIImage? {
        return UIImage(named: self.imageName)
    }
    
}
